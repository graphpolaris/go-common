package restfulDriverApi

import (
	"bytes"
	"encoding/json"
	"net/http"
	"os"

	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

var UnauthorizedError = errors.New("Unauthorized")

func GetUserManagerAPI(api string, target any) error {
	url := os.Getenv("USER_MANAGEMENT_SERVICE_API")
	if url == "" {
		url = "http://localhost:8000"
	}

	url = url + api
	// assure that api url does not ends on /
	if url[len(url)-1] == '/' {
		url = url[:len(url)-1]
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Error().Str("api", api).Msg("ERROR GetUserManagerAPI creating request")
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Str("api", api).Msg("ERROR GetUserManagerAPI do")
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		err = json.NewDecoder(resp.Body).Decode(&target)
		if err != nil {
			log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PostUserManagerAPI unmarshalling call to user management rest")
			return err
		}
		return nil
	} else {
		log.Error().Int("StatusCode", resp.StatusCode).Str("api", api).Msg("ERROR GetUserManagerAPI status code")
		return UnauthorizedError
	}
}

func PostUserManagerAPI(api string, body interface{}, target any) error {
	url := os.Getenv("USER_MANAGEMENT_SERVICE_API")
	if url == "" {
		url = "http://localhost:8000"
	}

	url = url + api
	// assure that api url does not ends on /
	if url[len(url)-1] == '/' {
		url = url[:len(url)-1]
	}

	jsonData, err := json.Marshal(body)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR PostUserManagerAPI marshalling call to user management rest")
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PostUserManagerAPI creating request")
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PostUserManagerAPI do")
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		err = json.NewDecoder(resp.Body).Decode(&target)
		if err != nil || target == nil {
			log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PostUserManagerAPI unmarshalling call to user management rest")
			return err
		}
		return nil
	} else {
		log.Error().Int("StatusCode", resp.StatusCode).Str("api", api).Msg("ERROR PostUserManagerAPI status code")
		return err
	}
}

func PutUserManagerAPI(api string, body interface{}, target any) error {
	url := os.Getenv("USER_MANAGEMENT_SERVICE_API")
	if url == "" {
		url = "http://localhost:8000"
	}

	url = url + api
	// assure that api url does not ends on /
	if url[len(url)-1] == '/' {
		url = url[:len(url)-1]
	}

	jsonData, err := json.Marshal(body)
	if err != nil {
		log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PutUserManagerAPI marshalling call to user management rest")
		return err
	}

	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PutUserManagerAPI creating request")
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PutUserManagerAPI do")
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		err = json.NewDecoder(resp.Body).Decode(&target)
		if err != nil {
			log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PostUserManagerAPI unmarshalling call to user management rest")
			return err
		}
		return nil
	} else {
		log.Error().Int("StatusCode", resp.StatusCode).Str("api", api).Msg("ERROR PutUserManagerAPI status code")
		return err
	}
}

func DeleteUserManagerAPI(api string, target any) error {
	url := os.Getenv("USER_MANAGEMENT_SERVICE_API")
	if url == "" {
		url = "http://localhost:8000"
	}

	url = url + api
	// assure that api url does not ends on /
	if url[len(url)-1] == '/' {
		url = url[:len(url)-1]
	}

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR DeleteUserManagerAPI creating request")
		return err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR DeleteUserManagerAPI do")
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 200 {
		err = json.NewDecoder(resp.Body).Decode(&target)
		if err != nil {
			log.Error().AnErr("ERROR", err).Str("api", api).Msg("ERROR PostUserManagerAPI unmarshalling call to user management rest")
			return err
		}
		return nil
	} else {
		log.Error().Int("StatusCode", resp.StatusCode).Str("api", api).Msg("ERROR DeleteUserManagerAPI status code")
		return err
	}
}

func ConvertInterface(origin any, target any) error {
	jsonDecoder, err := json.Marshal(origin)
	if err != nil {
		return err
	}
	err = json.Unmarshal(jsonDecoder, &target)
	if err != nil {
		return err
	}
	return nil
}
