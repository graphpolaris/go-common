package models

import (
	"github.com/rs/zerolog/log"
)

type ProjectModel struct {
	ID         string           `json:"id"`
	Name       string           `json:"name"`
	SaveStates []SaveStateModel `json:"saveStates"`
	Parent     *ProjectModel    `json:"parent"`
}

type SaveStateModel struct {
	ID             string                   `json:"id"`
	Name           string                   `json:"name"`
	DBConnections  []DBConnectionModel      `json:"dbConnections"`
	Queries        []QueryBuilderModel      `json:"queries"`
	Schemas        []SchemaModel            `json:"schemas"`
	Visualizations VisualizationModel       `json:"visualizations"`
	CreatedAt      string                   `json:"createdAt"`
	UserID         string                   `json:"userId"`
	UpdatedAt      string                   `json:"updatedAt"`
	Insights       []map[string]interface{} `json:"insights"`
	// Project        *ProjectModel            `json:"project"`
}

type DBType string

const (
	ArangoDB DBType = "arangodb"
	Neo4j    DBType = "neo4j"
)

type DBConnectionModel struct {
	ID                   int    `json:"id"`
	InternalDatabaseName string `json:"internalDatabaseName"`
	URL                  string `json:"url"`
	Protocol             string `json:"protocol"`
	Port                 int    `json:"port"`
	Username             string `json:"username"`
	Password             string `json:"password"`
	Type                 string `json:"type"`
}

func (db *DBConnectionModel) GetDBType() DBType {
	switch db.Type {
	case "arangodb":
		return ArangoDB
	case "neo4j":
		return Neo4j
	}
	return Neo4j
}

type SchemaModel struct {
	Settings map[string]interface{} `json:"settings" bson:"settings"`
}

type QueryBuilderModel struct {
	Graph                map[string]interface{}   `json:"graph"`
	Settings             map[string]interface{}   `json:"settings"`
	AttributesBeingShown []map[string]interface{} `json:"attributesBeingShown"`
}

type VisualizationModel struct {
	ActiveVisualizationIndex int                      `json:"activeVisualizationIndex"`
	OpenVisualizationArray   []map[string]interface{} `json:"openVisualizationArray"`
}

func (ss *SaveStateModel) Enforce(user string, item string, action string) bool {
	allowed, err := CheckPolicies(user, []PolicyItem{
		{Obj: ss.ID + "/" + item, Act: action},
	})

	if err != nil {
		log.Error().Err(err).Msg("error enforcing policy")
		return false
	}

	return allowed[0]
}

func (ss *SaveStateModel) EnforceBatch(user string, items []PolicyItem) []bool {
	for i := range items {
		items[i].Obj = ss.ID + "/" + items[i].Obj
	}
	allowed, err := CheckPolicies(user, items)

	if err != nil {
		log.Error().Err(err).Msg("error enforcing policy")
		retFalse := make([]bool, len(allowed))
		for i := range retFalse {
			retFalse[i] = false
		}
		return retFalse
	}

	return allowed
}
