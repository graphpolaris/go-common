/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package models

import (
	"errors"
	"fmt"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/restfulDriverApi"
	"github.com/rs/zerolog/log"
)

type InsightRequestModel struct {
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Recipients  []string `json:"recipients"`
	Frequency   string   `json:"frequency"`
	Template    string   `json:"template"`
	SaveStateId string   `json:"saveStateId"`
	Type        string   `json:"type"`
}

type InsightModel struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Recipients  []string `json:"recipients"`
	Frequency   string   `json:"frequency"`
	Template    string   `json:"template"`
	SaveStateID string   `json:"saveStateId"`
	Type        string   `json:"type"`
}

func GetInsight(userID string, insightID string) (*InsightModel, error) {
	var decoder map[string]interface{}
	err := restfulDriverApi.GetUserManagerAPI(fmt.Sprintf("/api/insight/%s/%s", userID, insightID), &decoder)
	if err != nil || decoder == nil {
		log.Error().Err(err).Msg("ERROR GetInsight in user-management-service")
		return nil, err
	}

	var insight InsightModel
	if userID != decoder["userId"].(string) {
		return nil, UnauthorizedError()
	}
	err = restfulDriverApi.ConvertInterface(decoder, &insight)
	if err != nil {
		log.Error().Err(err).Msg("ERROR GetInsight unmarshalling insight")
		return nil, err
	}
	return &insight, nil
}

func CreateInsight(userID string, insightRequest *InsightRequestModel) (*InsightModel, error) {
	var decoder map[string]interface{}
	err := restfulDriverApi.PostUserManagerAPI(fmt.Sprintf("/api/insight/create/%s/%s", userID, insightRequest.SaveStateId), insightRequest, &decoder)
	if err != nil || decoder == nil {
		log.Error().Err(err).Msg("ERROR creating insight in user-management-service")
		return nil, err
	}

	decoder["id"] = fmt.Sprint(decoder["id"].(float64))
	var insight InsightModel
	err = restfulDriverApi.ConvertInterface(decoder, &insight)
	if err != nil {
		log.Error().Err(err).Msg("ERROR CreateInsight unmarshalling insight")
		return nil, err
	}

	return &insight, nil
}

func UpdateInsight(userID string, insightId string, insightRequest *InsightRequestModel) (*InsightModel, error) {
	var decoder map[string]interface{}
	err := restfulDriverApi.PutUserManagerAPI(fmt.Sprintf("/api/insight/update/%s/%s", userID, insightId), insightRequest, &decoder)
	if err != nil || decoder == nil {
		log.Error().Err(err).Msg("ERROR updating insight in user-management-service")
		return nil, err
	}

	decoder["id"] = fmt.Sprint(decoder["id"].(float64))
	var insight InsightModel
	err = restfulDriverApi.ConvertInterface(decoder, &insight)
	if err != nil {
		log.Error().Err(err).Msg("ERROR UpdateInsight unmarshalling insight")
		return nil, err
	}

	return &insight, nil
}

func DeleteInsight(userID string, insightID string) error {
	var decoder map[string]interface{}
	err := restfulDriverApi.DeleteUserManagerAPI(fmt.Sprintf("/api/insight/delete/%s/%s", userID, insightID), &decoder)
	if err != nil || decoder == nil {
		log.Error().Err(err).Msg("ERROR deleting insight in user-management-service")
		return err
	}

	var res map[string]bool
	err = restfulDriverApi.ConvertInterface(decoder, &res)
	if err != nil {
		log.Error().Err(err).Msg("ERROR DeleteInsight unmarshalling response")
		return err
	}

	if !res["success"] {
		log.Error().Msg("ERROR deleting insight")
		return errors.New("ERROR deleting insight")
	}

	log.Trace().Str("userID", userID).Str("insightID", insightID).Msg("deleted insight")
	return nil
}

func GetInsights(userID string, saveStateID string) ([]InsightModel, error) {
	var decoder []map[string]interface{}
	err := restfulDriverApi.GetUserManagerAPI(fmt.Sprintf("/api/insight/%s/all/%s/", userID, saveStateID), &decoder)
	if err != nil || decoder == nil {
		log.Error().Err(err).Msg("ERROR getting insights in user-management-service")
		return nil, err
	}

	var insights []InsightModel
	for _, item := range decoder {
		var insight InsightModel
		item["id"] = fmt.Sprint(item["id"].(float64))
		err := restfulDriverApi.ConvertInterface(item, &insight)
		if err != nil {
			log.Error().Err(err).Msg("ERROR unmarshalling insight")
			return nil, err
		}
		insights = append(insights, insight)
	}

	return insights, nil
}
