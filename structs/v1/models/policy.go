package models

import (
	"fmt"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/restfulDriverApi"
	"github.com/rs/zerolog/log"
)

// new custom error
type PolicyUnauthorized struct {
	message *string
}

type PolicyMap map[string]map[string]bool

func UnauthorizedError(a ...any) error {
	s := ""
	if len(a) == 0 {
		return &PolicyUnauthorized{message: &s}
	}

	if len(a) == 1 {
		s = fmt.Sprintf(a[0].(string))
		return &PolicyUnauthorized{message: &s}
	}

	if len(a) > 1 {
		s = fmt.Sprintf(a[0].(string), a[1:]...)
		return &PolicyUnauthorized{message: &s}
	}

	return &PolicyUnauthorized{message: &s}
}

func (r *PolicyUnauthorized) Error() string {
	return fmt.Sprintf("unauthorized: %v", r.message)
}

type PolicyItem struct {
	Obj string
	Act string
}

const R = "R"
const W = "W"
const ALL = "*"
const SAVESTATE = "savestate"
const DEMO_USER = "demoUser"
const DATABASE = "database"
const QUERY = "query"
const VISUALIZATION = "visualization"

func CheckPolicies(userID string, items []PolicyItem) ([]bool, error) {

	body := []map[string]string{}

	for _, item := range items {
		body = append(body, map[string]string{"sub": userID, "obj": item.Obj, "act": item.Act})
	}

	var decoder []bool
	err := restfulDriverApi.PostUserManagerAPI("/api/policy/batch", body, &decoder)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR CheckPolicies in user-management-service rest")
		return nil, err
	}

	return decoder, nil
}
