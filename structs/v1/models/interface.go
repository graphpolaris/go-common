package models

import "encoding/json"

type ObjectID [12]byte

type SessionData struct {
	Username      string `json:"username"`
	UserID        string `json:"userID"`
	ImpersonateID string `json:"impersonateID"`
	SessionID     string `json:"sessionID"`
	SaveStateID   string `json:"saveStateID"`
	RoomID        string `json:"roomID"`
	JWT           string `json:"jwt"`
}

type RouterCache struct {
	QueueID  string
	UserID   string
	Username string
}

type RoomCache struct {
	RoomID   string
	Sessions []string
}

func (i RouterCache) MarshalBinary() ([]byte, error) {
	return json.Marshal(i)
}
