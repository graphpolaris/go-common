/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package models

import (
	"errors"
)

/*
GetDatabaseInfo opens a gRPC connection to the user management service and retrieves the database info for the given client and database name

	userID: *string, the ID of the client
	databaseName: *string, the name of the database
	Return: (*entity.DatabaseInfo, error), the info of the database, and and error if something is wrong
*/
func GetDatabaseInfo(userID *string, saveStateID *string) (DBConnectionModel, error) {
	ss, err := GetSaveState(userID, saveStateID)
	if err != nil {
		return DBConnectionModel{}, err
	}

	if ss.DBConnections == nil || len(ss.DBConnections) == 0 {
		return DBConnectionModel{}, errors.New("No saveState/dbconnection found")
	}

	return ss.DBConnections[0], nil
}

func GetDatabaseType(userID *string, saveStateID *string) (DBType, error) {
	ss, err := GetSaveState(userID, saveStateID)
	if err != nil {
		return Neo4j, err
	}

	if ss.DBConnections == nil || len(ss.DBConnections) == 0 {
		return Neo4j, errors.New("No saveState/dbconnection found")
	}

	return ss.DBConnections[0].GetDBType(), nil
}
