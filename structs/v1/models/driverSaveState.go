/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package models

import (
	"errors"
	"fmt"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/restfulDriverApi"
	"github.com/rs/zerolog/log"
)

func GetSaveState(userID *string, saveStateID *string) (*SaveStateModel, error) {
	var decoder map[string]interface{}
	err := restfulDriverApi.GetUserManagerAPI("/api/user_save_state/"+*userID+"/"+*saveStateID, &decoder)
	if err != nil || decoder == nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR GetSaveState in user-management-service rest get ss")
		return nil, err
	}

	var saveStateModel SaveStateModel
	decoder["userId"] = fmt.Sprint(decoder["userId"].(float64))
	err = restfulDriverApi.ConvertInterface(decoder, &saveStateModel)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR GetSaveState unmarshalling ss")
		return nil, err
	}
	return &saveStateModel, nil
}

func GetSaveStates(userID *string) ([]SaveStateModel, error) {
	var decoder []map[string]interface{}
	if userID == nil || *userID == "" {
		log.Error().Msg("ERROR GetSaveStates userID is nil")
		return nil, errors.New("ERROR GetSaveStates userID is nil")
	}
	err := restfulDriverApi.GetUserManagerAPI("/api/user_save_state/"+*userID, &decoder)
	if err != nil || decoder == nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR GetSaveStates in user-management-service rest get sss")
	}

	var saveStateModel []SaveStateModel

	for i := 0; i < len(decoder); i++ {
		decoder[i]["userId"] = fmt.Sprint(decoder[i]["userId"].(float64))
	}
	err = restfulDriverApi.ConvertInterface(decoder, &saveStateModel)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR GetSaveStates unmarshalling ss")
		return nil, err
	}
	return saveStateModel, nil
}

func CreateSaveState(userID *string, saveStateModel *SaveStateModel) (*SaveStateModel, error) {
	var decoder map[string]interface{}
	err := restfulDriverApi.PostUserManagerAPI("/api/save_state", saveStateModel, &decoder)
	if err != nil || decoder == nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR creating user in user-management-service rest create ss")
		return nil, err
	}

	decoder["userId"] = fmt.Sprint(decoder["userId"].(float64))
	err = restfulDriverApi.ConvertInterface(decoder, &saveStateModel)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR CreateSaveState unmarshalling ss")
		return nil, err
	}

	return saveStateModel, nil
}

func UpdateSaveState(userID *string, saveStateModel *SaveStateModel) (*SaveStateModel, error) {
	var decoder map[string]interface{}
	err := restfulDriverApi.PutUserManagerAPI("/api/save_state/"+string(saveStateModel.ID), saveStateModel, &decoder)
	if err != nil || decoder == nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR creating user in user-management-service rest update ss")
		return nil, err
	}

	decoder["userId"] = fmt.Sprint(decoder["userId"].(float64))
	err = restfulDriverApi.ConvertInterface(decoder, &saveStateModel)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR UpdateSaveState unmarshalling ss")
		return nil, err
	}

	return saveStateModel, nil
}

func DeleteSaveState(userID *string, saveStateID *string) error {
	var decoder map[string]interface{}
	err := restfulDriverApi.DeleteUserManagerAPI("/api/save_state/"+*saveStateID, &decoder)
	if err != nil || decoder == nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR creating user in user-management-service rest delete ss")
		return err
	}

	var res map[string]bool
	err = restfulDriverApi.ConvertInterface(decoder, &res)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR DeleteSaveState unmarshalling ss")
		return err
	}

	if !res["success"] {
		log.Error().Str("method", "DeleteSaveState").Msg("ERROR deleting saveState")
		return errors.New("ERROR deleting saveState")
	}

	log.Trace().Str("userID", *userID).Str("saveStateID", *saveStateID).Msg("deleted saveState")
	return nil
}

type RequestShareSaveState struct {
	SaveStateID string                        `json:"saveStateId"`
	Users       []RequestShareSaveStateToUser `json:"users"`
}
type RequestShareSaveStateToUser struct {
	UserID  string                 `json:"userId"` // can also be * for all users
	Sharing map[string]interface{} `json:"sharing"`
}

func ShareSaveState(userID *string, shareArray *RequestShareSaveState) error {
	log.Trace().Str("userID", *userID).Str("saveStateID", shareArray.SaveStateID).Any("shareArray", shareArray).Msg("sharing saveState")
	var decoder map[string]interface{}
	err := restfulDriverApi.PostUserManagerAPI("/api/save_state/"+string(shareArray.SaveStateID)+"/share", shareArray, &decoder)
	if err != nil || decoder == nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR making share save state in user-management-service rest")
		return err
	}

	return nil
}
