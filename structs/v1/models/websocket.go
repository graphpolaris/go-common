package models

// WS communication
type SocketInKeyType string
type SocketInSubKeyType string

const (
	BroadcastStateKey SocketInKeyType = "broadcastState"
	DBConnectionKey   SocketInKeyType = "dbConnection"
	SchemaKey         SocketInKeyType = "schema"
	QueryKey          SocketInKeyType = "query"
	StateKey          SocketInKeyType = "state"
	UserKey           SocketInKeyType = "user"
	InsightKey        SocketInKeyType = "insight"
)

const (
	// Crud Operations
	Create SocketInSubKeyType = "create"
	Get    SocketInSubKeyType = "get"
	GetAll SocketInSubKeyType = "getAll"
	Update SocketInSubKeyType = "update"
	Delete SocketInSubKeyType = "delete"
	Select SocketInSubKeyType = "select"

	// Custom
	NewDbConnectionKey     SocketInSubKeyType = "newDbConnection"
	EditDbConnectionKey    SocketInSubKeyType = "editDbConnection"
	DeleteDbConnectionKey  SocketInSubKeyType = "deleteDbConnection"
	GetDbConnectionKey     SocketInSubKeyType = "getDbConnection"
	GetAllDbConnectionsKey SocketInSubKeyType = "getAllDbConnections"
	TestConnectionKey      SocketInSubKeyType = "testConnection"
	PolicyCheck            SocketInSubKeyType = "policyCheck"
	GetPolicy              SocketInSubKeyType = "getPolicy"
	ShareState             SocketInSubKeyType = "shareState"

	GetSchemaStatsKey SocketInSubKeyType = "getSchemaStats"

	RunQueryKey SocketInSubKeyType = "runQuery"
)

type FromFrontend struct {
	CallID string             `json:"callID"`
	Key    SocketInKeyType    `json:"key"`
	SubKey SocketInSubKeyType `json:"subKey"`
	Body   string             `json:"body"`
}

type MessageData struct {
	FromFrontend FromFrontend `json:"fromFrontend"`
	SessionData  *SessionData `json:"sessionData"`
}

// Message describes the format of messages being sent to the frontend
type ToFrontend struct {
	CallID string      `json:"callID"`
	Type   string      `json:"type"`
	Status string      `json:"status"`
	Value  interface{} `json:"value"`
}

/*
MessageValueQuery describes outgoing messages from the query service
*/
type MessageValueQuery struct {
	QueryID string      `json:"queryID"`
	Result  interface{} `json:"result"`
}

/*
MessageStruct describes outgoing messages from the query service
TODO: needs to generalize so that ML can handle other types of requests, not just requests based on query results
*/
type MessageML struct {
	QueryID      string      `json:"queryID"`
	Type         string      `json:"type"`
	Value        interface{} `json:"value"`
	MLAttributes interface{} `json:"mlAttributes"`
}

type MessageSchemaInference struct {
	QueryID      string      `json:"queryID"`
	DatabaseInfo interface{} `json:"databaseInfo"`
}
