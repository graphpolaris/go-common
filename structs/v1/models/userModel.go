package models

import (
	"github.com/rs/zerolog/log"
)

type UserModel struct {
	ID         string           `json:"id"`
	Username   string           `json:"username"`
	SaveStates []SaveStateModel `json:"saveStates"`
	Projects   []ProjectModel   `json:"projects"`
}

func (um *UserModel) Enforce(user string, item string, action string) bool {
	allowed, err := CheckPolicies(user, []PolicyItem{
		{Obj: item, Act: action},
	})

	if err != nil {
		log.Error().Err(err).Msg("error enforcing policy")
		return false
	}

	if len(allowed) == 0 {
		return false
	}
	return allowed[0]
}
