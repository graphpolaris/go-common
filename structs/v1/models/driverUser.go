/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package models

import (
	"fmt"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/restfulDriverApi"
	"github.com/rs/zerolog/log"
)

func CreateUserIfNotExistsByName(username string) (UserModel, error) {
	log.Trace().Str("username", username).Msg("creating user")
	var userModel UserModel

	var decoder map[string]interface{}
	err := restfulDriverApi.PostUserManagerAPI("/api/user/", map[string]string{
		"username": username,
	}, &decoder)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR CreateUserIfNotExistsByName in user-management-service rest")
		return UserModel{}, err
	}
	log.Trace().Interface("decoder", decoder).Msg("decoded message from ums")

	decoder["id"] = fmt.Sprint(decoder["id"].(float64))
	if len(decoder["saveStates"].([]interface{})) > 0 {
		for i := 0; i < len(decoder["saveStates"].([]interface{})); i++ {
			d := decoder["saveStates"].([]interface{})[i].(map[string]interface{})
			d["userId"] = fmt.Sprint(d["userId"].(float64))
		}
	}

	err = restfulDriverApi.ConvertInterface(decoder, &userModel)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR CreateUserIfNotExistsByName unmarshalling userModel")
		return UserModel{}, err
	}

	return userModel, nil
}

func GetUser(userID string) (*UserModel, error) {
	log.Trace().Str("userID", userID).Msg("getting user")
	var userModel UserModel

	var decoder map[string]interface{}
	err := restfulDriverApi.GetUserManagerAPI("/api/user/"+userID, &decoder)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR GetUser in user-management-service rest")
		return nil, err
	}

	decoder["id"] = fmt.Sprint(decoder["id"].(float64))
	if len(decoder["saveStates"].([]interface{})) > 0 {
		for i := 0; i < len(decoder["saveStates"].([]interface{})); i++ {
			d := decoder["saveStates"].([]interface{})[i].(map[string]interface{})
			d["userId"] = fmt.Sprint(d["userId"].(float64))
		}
	}
	err = restfulDriverApi.ConvertInterface(decoder, &userModel)
	if err != nil {
		log.Error().AnErr("ERROR", err).Msg("ERROR CreateUserIfNotExistsByName unmarshalling userModel")
		return nil, err
	}

	return &userModel, nil
}
