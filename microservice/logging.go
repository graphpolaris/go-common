package microservice

import (
	"os"
	"strconv"

	"github.com/christinoleo/alice"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func SetupLogging() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	logLevel, log_err := strconv.Atoi(os.Getenv("LOG_LEVEL"))
	if log_err != nil {
		log.Error().AnErr("err", log_err).Msg("LOG_LEVEL not type int or empty")
	}
	zerolog.SetGlobalLevel(zerolog.Level(logLevel))
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	alice.SetLogLevel(-1) // TODO: check if needed
}
