/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package microservice

/*
Interface is an interface for a key value storage
*/
type Interface interface {
	SetupLogging() error
}
