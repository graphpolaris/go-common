package microservice

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
)

// API describes all methods an API instance should have
type API interface {
	Start(port int, r chi.Router)
	StartTTL(port int, tlsCert string, tlsKey string, r chi.Router)
	Routes() chi.Router
	RoutesWithoutHeaders() chi.Router
	GetUserDetailsFromHeaders(expectHeaders bool) func(next http.Handler) http.Handler
	GetUserIDKey() requestContextKey
	GetSessionIDKey() requestContextKey
}

type requestContextKey int

// api implements the API interface
const (
	userIDKey requestContextKey = iota
	sessionIDKey
	roomIDKey
	jwtKey
	signinProviderKey
	callbackKey
)

type api struct {
}

// New creates a new API instance
func New() API {
	return &api{}
}

func ContextToSessionData(r *http.Request) (models.SessionData, error) {
	ctx := r.Context()

	userID, ok := ctx.Value(userIDKey).(string)
	if !ok {
		log.Error().Msg("userID not found in context")
		return models.SessionData{}, errors.New("userID not found in context")
	}

	sessionID, ok := ctx.Value(sessionIDKey).(string)
	if !ok {
		log.Error().Msg("sessionID not found in context")
		return models.SessionData{}, errors.New("sessionID not found in context")
	}

	sessionData := models.SessionData{
		UserID:    userID,
		SessionID: sessionID,
	}

	return sessionData, nil
}

func (a *api) Start(port int, r chi.Router) {
	log.Info().Int("port", port).Msg("starting API")
	http.ListenAndServe(fmt.Sprintf(":%d", port), r)
}

func (a *api) StartTTL(port int, tlsCert string, tlsKey string, r chi.Router) {
	log.Info().Int("port", port).Msg("starting API")
	http.ListenAndServeTLS(fmt.Sprintf(":%d", port), tlsCert, tlsKey, r)
}

// Routes contains all the routes exposed by the API
func (a *api) Routes() chi.Router {
	r := a.RoutesWithoutHeaders()
	r.Use(a.GetUserDetailsFromHeaders(os.Getenv("DEV") != "true"))

	return r
}

func (a *api) RoutesWithoutHeaders() chi.Router {
	r := chi.NewRouter()

	// for more ideas, see: https://developer.github.com/v3/#cross-origin-resource-sharing
	AllowedOrigins := strings.Split(os.Getenv("ALLOWED_ORIGINS"), ",")
	log.Trace().Strs("AllowedOrigins", AllowedOrigins).Msg("")

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   AllowedOrigins,
		AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token", "Access-Control-Allow-Origin", "Allow-Origin-Header", "userid", "sessionid"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
		// Debug:            true,
	}))

	r.Use(middleware.Recoverer)
	r.Use(middleware.Logger)

	return r
}

func getFromHeader(r *http.Request, key string, defaultVal string) (string, bool) {
	if val, ok := r.Header[key]; ok {
		return val[0], true
	}
	if defaultVal != "" {
		return defaultVal, false
	}
	return "", false
}

func GetSessionDataFromHeader(r *http.Request, expectHeaders bool) (models.SessionData, bool, error) {
	sessionData := models.SessionData{
		Username:      "",
		UserID:        "",
		SessionID:     "",
		RoomID:        "",
		JWT:           "",
		ImpersonateID: "",
	}

	foundAll := true

	if !expectHeaders {
		sessionData.Username = "Username"
		sessionData.UserID = "1"
		sessionData.SessionID = uuid.New().String()
		sessionData.RoomID = ""
		sessionData.JWT = "JWT"
		sessionData.ImpersonateID = ""
	}

	notFoundError := errors.New("not found")
	emailHeaderVariable := os.Getenv("USER_HEADER_VARIABLE")
	if emailHeaderVariable == "" {
		emailHeaderVariable = "X-Authentik-Email"
	}
	Username, found := getFromHeader(r, emailHeaderVariable, sessionData.Username)
	if !found && expectHeaders {
		log.Error().AnErr("Error", notFoundError).Msg("error reading Username/Email")
		return sessionData, false, notFoundError
	} else if !found {
		foundAll = false
	}

	UserID, found := getFromHeader(r, "X-Authentik-Uid", sessionData.UserID)
	if !found && expectHeaders {
		log.Error().AnErr("Error", notFoundError).Msg("error reading userID")
		return sessionData, false, notFoundError
	} else if !found {
		foundAll = false
	}

	SessionID, found := getFromHeader(r, "Sessionid", sessionData.SessionID)
	if !found {
		foundAll = false
	}

	ImpersonateID, found := getFromHeader(r, "Impersonateid", sessionData.ImpersonateID)
	if !found {
		ImpersonateID = r.URL.Query().Get("impersonateID")
		log.Debug().Str("ImpersonateID", ImpersonateID).Any("asd", r.URL.Query()).Msg("Using impersonateID from URL parameter")
	}

	RoomID, _ := getFromHeader(r, "RoomID", sessionData.RoomID)
	if !found {
		foundAll = false
	}
	// if err != nil {
	// 	log.Error().AnErr("Error", err).Msg("error reading RoomID")
	// 	return sessionData, err
	// } else {
	// 	log.Debug().Str("RoomID", sessionData.RoomID).Msg("found RoomID")
	// }

	JWT, found := getFromHeader(r, "X-Authentik-Jwt", sessionData.JWT)
	if !found && expectHeaders {
		log.Error().AnErr("Error", notFoundError).Msg("error reading JWT")
		return sessionData, false, notFoundError
	} else if !found {
		foundAll = false
	}

	sessionData.Username = Username
	sessionData.UserID = UserID
	sessionData.SessionID = SessionID
	sessionData.RoomID = RoomID
	sessionData.JWT = JWT
	sessionData.ImpersonateID = ImpersonateID

	// If save state id is set, check permissions
	// ImpersonateID is a dev-only field to test this out in DEV environment
	if sessionData.ImpersonateID != "" && sessionData.ImpersonateID != sessionData.UserID && sessionData.UserID != "" {
		user, err := models.GetUser(sessionData.UserID)
		if err != nil {
			log.Error().AnErr("Err", err).Msg("error getting user")
			return sessionData, false, err
		}

		// for now, only allow those with write permissions to impersonate the demo user
		if !user.Enforce(sessionData.UserID, models.DEMO_USER, models.W) {
			log.Debug().Str("impersonateID", sessionData.ImpersonateID).Msg("impersonateID not allowed for user")
			return sessionData, false, errors.New("impersonateID not allowed")
		}

		log.Debug().Str("impersonateID", sessionData.ImpersonateID).Msg("impersonateID allowed for user")
		sessionData.UserID = "0" // impersonated
		sessionData.Username = "demoUser"
	}

	return sessionData, foundAll, nil
}

func (a *api) GetUserDetailsFromHeaders(expectHeaders bool) func(next http.Handler) http.Handler {
	middleware := func(next http.Handler) http.Handler {
		handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			log.Trace().Msg("checking for headers")

			sessionData, _, err := GetSessionDataFromHeader(r, expectHeaders)
			if err != nil {
				http.Error(w, err.Error(), http.StatusForbidden)
				return
			}
			log.Debug().Any("sessionData", sessionData).Msg("found session data")

			// Get Userid from headers and add to request context
			ctx = context.WithValue(ctx, userIDKey, sessionData.UserID)

			// Get Userid from headers and add to request context
			ctx = context.WithValue(ctx, roomIDKey, sessionData.RoomID)

			// Get Sessionid from headers and add to request context
			ctx = context.WithValue(ctx, sessionIDKey, sessionData.SessionID)

			// Get Sessionid from headers and add to request context
			ctx = context.WithValue(ctx, jwtKey, sessionData.JWT)

			// Serve next handler
			next.ServeHTTP(w, r.WithContext(ctx))
		})
		return handler
	}

	return middleware
}

func (a *api) GetUserIDKey() requestContextKey {
	return userIDKey
}

func (a *api) GetSessionIDKey() requestContextKey {
	return sessionIDKey
}

func (a *api) GetSigninProviderKey() requestContextKey {
	return signinProviderKey
}

func (a *api) GetCallbackKey() requestContextKey {
	return callbackKey
}
