package signin

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/rs/zerolog/log"
)

// googleProvider implements the Provider interface for Google signin's
type googleProvider struct {
}

func (p *googleProvider) GetUserInfo(code, state string) (userInfo ProvidedUserInfo, err error) {
	log.Trace().Str("provider", "google").Str("code", code).Msg("getting user info")

	userInfo = ProvidedUserInfo{}

	// Compare states
	if state != "somestate" {
		return userInfo, errors.New("states don't match")
	}

	// Exchange code for an access token
	token, err := GoogleOAuthConfig.Exchange(context.TODO(), code)
	if err != nil {
		log.Error().Err(err).Msg("failed to exchange code for access token")
		return userInfo, err
	}

	// Get user info using access token
	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		log.Error().Err(err).Msg("failed to get user info")
		return userInfo, err
	}
	defer response.Body.Close()

	// convert JSON to string map
	var result map[string]interface{}
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode response body")
		return
	}

	userInfo.Email = result["email"].(string)
	userInfo.Name = result["name"].(string)
	userInfo.ID = result["id"].(string)
	userInfo.Provider = Google

	log.Trace().Interface("userInfo", userInfo).Msg("received google user info")

	return
}
