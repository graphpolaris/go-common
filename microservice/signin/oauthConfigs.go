package signin

import (
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/github"
	"golang.org/x/oauth2/google"
)

// This file contains all OAuth 2 configs

var (
	// GoogleOAuthConfig contains the OAuth2 config for Google
	GoogleOAuthConfig = &oauth2.Config{
		ClientID:     os.Getenv("GOOGLE_CLIENT_ID"),
		ClientSecret: os.Getenv("GOOGLE_CLIENT_SECRET"),
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
			"openid",
		},
		RedirectURL: os.Getenv("GOOGLE_CALLBACK_URL"),
		Endpoint:    google.Endpoint,
	}

	// GithubOAuthConfig contains the OAuth2 config for Github
	GithubOAuthConfig = &oauth2.Config{
		ClientID:     os.Getenv("GITHUB_CLIENT_ID"),
		ClientSecret: os.Getenv("GITHUB_CLIENT_SECRET"),
		RedirectURL:  os.Getenv("GITHUB_CALLBACK_URL"),
		Scopes: []string{
			"read:user",
		},
		Endpoint: github.Endpoint,
	}
)
