package signin

import (
	"context"
	"errors"
	"strconv"

	"github.com/google/go-github/github"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
)

// githubProvider implements the Provider interface for Github signin's
type githubProvider struct {
}

// TokenSource is a type that contains the access token, which is needed to fetch user data from GitHub
type TokenSource struct {
	AccessToken string
}

// Token creates a useable token from tokenSource
func (t *TokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}
	return token, nil
}

func (p *githubProvider) GetUserInfo(code, state string) (userInfo ProvidedUserInfo, err error) {
	log.Trace().Str("provider", "github").Str("code", code).Msg("getting user info")

	userInfo = ProvidedUserInfo{}

	// Compare states
	if state != "somestate" {
		return userInfo, errors.New("states don't match")
	}

	// Exchange code for an access token
	token, err := GithubOAuthConfig.Exchange(context.TODO(), code)
	if err != nil {
		log.Error().Err(err).Msg("failed to exchange code for access token")
		return userInfo, err
	}

	// Create Token source
	tokenSource := &TokenSource{
		AccessToken: token.AccessToken,
	}
	oauthClient := oauth2.NewClient(context.TODO(), tokenSource)
	client := github.NewClient(oauthClient)
	response, _, err := client.Users.Get(context.TODO(), "")
	if err != nil {
		log.Error().Err(err).Msg("failed to get user info")
	}

	userInfo.Email = response.GetEmail()
	userInfo.Name = response.GetName()
	userInfo.ID = strconv.FormatInt(response.GetID(), 10)
	userInfo.Provider = Github

	log.Trace().Interface("userInfo", userInfo).Msg("received github user info")

	return
}
