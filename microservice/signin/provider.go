package signin

// ProviderKey encodes a login provider like google and github
type ProviderKey int

const (
	Google ProviderKey = iota + 1
	Github
	End
)

func (lp ProviderKey) String() string {
	return []string{"Google", "Github"}[lp-1]
}

// ProvidedUserInfo contains user info provided by a provider like Google or Github
type ProvidedUserInfo struct {
	Name     string
	Email    string
	ID       string
	Provider ProviderKey
}

// A Provider provides signin functionality for a provider like Google or Github
// it uses the given code to get an access token and retrieve user info we need, like email, name and id
type Provider interface {
	GetUserInfo(code, state string) (userInfo ProvidedUserInfo, err error)
}

// New creates a new signin provider based on the requested provider
func New(provider ProviderKey) Provider {
	switch provider {
	case Google:
		return &googleProvider{}
	case Github:
		return &githubProvider{}
	default:
		return &googleProvider{}
	}
}
