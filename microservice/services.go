package microservice

import (
	"os"
	"strconv"
	"time"

	"git.science.uu.nl/graphpolaris/go-common/broker"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
	"github.com/rs/zerolog/log"
)

func ConnectRabbit() broker.Interface {
	port, err := strconv.Atoi(os.Getenv("RABBIT_PORT"))
	if err != nil {
		log.Panic().AnErr("err", err).Msg("invalid rabbitmq port")
		return nil
	}
	brokerDriver := broker.NewDriver(os.Getenv("RABBIT_USER"), os.Getenv("RABBIT_PASSWORD"), os.Getenv("RABBIT_HOST"), port)
	log.Info().Str("rabbitUser", os.Getenv("RABBIT_USER")).Msg("")

	return brokerDriver
}

func ConnectRedis() keyvaluestore.Interface {
	log.Info().Msg("connecting to redis")
	redisService := keyvaluestore.NewDriver()
	err := redisService.Connect(os.Getenv("REDIS_ADDRESS"), os.Getenv("REDIS_PASSWORD"))
	if err != nil {
		log.Error().Str("address", os.Getenv("REDIS_ADDRESS")).AnErr("Error", err).Msg("error connecting to redis. Retrying in 5 seconds...")
		// Retry
		time.Sleep(time.Duration(5) * time.Second)
		return ConnectRedis()
	} else {
		log.Info().Str("address", os.Getenv("REDIS_ADDRESS")).Msg("successfully connected to redis")
	}
	return redisService
}
