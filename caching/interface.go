/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package caching

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
	"github.com/rs/zerolog/log"
)

type Caching struct {
	Key                          string
	KeyValueStore                keyvaluestore.Interface
	RetrievalDurationEnvVariable string
	CacheMissCallback            func(request []byte, message *models.MessageData) (data interface{}, err error)
	CacheHitCallback             func(data interface{}, message *models.MessageData) error
}

func New(Key string, KeyValueStore keyvaluestore.Interface, RetrievalDurationEnvVariable string) *Caching {
	return &Caching{
		Key:                          Key,
		KeyValueStore:                KeyValueStore,
		RetrievalDurationEnvVariable: RetrievalDurationEnvVariable,
	}
}

func (c *Caching) SetWithEnvDuration(ctx context.Context, key string, value interface{}, expirationEnvVar string, expirationFallback time.Duration) error {
	return c.KeyValueStore.SetWithEnvDuration(ctx, key, value, expirationEnvVar, expirationFallback)
}

/*
 * Retrieve retrieves the data from the cache or the database. It calls the Callbacks in case of cache hit or miss.
 * In the case of cache misses, you need to return the data to be stored in cache from it.
 *
 * 	request: *[]byte, the request
 * 	keySuffix: string, the suffix of the key
 * 	allowCached: bool, whether or not to allow cached data
 * 	sessionData: models.SessionData, the session data
 * 	checkIfAlreadyBeingRetrieved: bool, whether or not to check if the data is already being retrieved
 * 	Return: error, returns an error in the case something goes wrong
 */
func (c *Caching) RetrieveAsync(request *[]byte, keySuffix string, allowCached bool, message *models.MessageData, checkIfAlreadyBeingRetrieved bool) {
	go func() {
		err := c.retrieve(request, keySuffix, allowCached, message, checkIfAlreadyBeingRetrieved)
		if err != nil {
			log.Error().AnErr("Err", err).Any("data", message.SessionData).Msg("error retrieving schema")
		}
	}()
}

// Helper function to standardize retrieval of routing key for messaging logic
func (c *Caching) GetRouting(sessionID string) (models.RouterCache, error) {
	getClientQueueIDContext, cancelGetClientQueueID := context.WithTimeout(context.Background(), time.Second*10)
	defer cancelGetClientQueueID()

	data, err := c.KeyValueStore.Get(getClientQueueIDContext, fmt.Sprintf("routing %s", sessionID), keyvaluestore.String)
	if err != nil {
		return models.RouterCache{}, err
	}
	if data == nil {
		return models.RouterCache{}, fmt.Errorf("no routing found for session %s", sessionID)
	}
	ret := models.RouterCache{}
	json.Unmarshal([]byte(data.(string)), &ret)

	c.KeyValueStore.ExtendExpiration(getClientQueueIDContext, fmt.Sprintf("routing %s", sessionID), c.RetrievalDurationEnvVariable, time.Minute)
	return ret, nil
}

// Helper function to standardize setter of routing key for messaging logic
func (c *Caching) SetRouting(sessionID string, value models.RouterCache) error {
	log.Trace().Any("value", value).Msgf("Setting routing for session %s", sessionID)
	return c.KeyValueStore.SetWithEnvDuration(context.Background(), fmt.Sprintf("routing %s", sessionID), value, c.RetrievalDurationEnvVariable, time.Minute)
}

// Helper function to standardize setter of routing key for messaging logic
func (c *Caching) SetRoutingIfNotExist(sessionID string, value models.RouterCache) error {
	log.Trace().Any("value", value).Msgf("Setting routing for session %s", sessionID)
	data, err := c.KeyValueStore.Get(context.Background(), fmt.Sprintf("routing %s", sessionID), keyvaluestore.String)
	if err != nil && err != keyvaluestore.ErrKeyNotFound {
		return err
	}
	if err == keyvaluestore.ErrKeyNotFound || data == nil {
		return nil
	}

	return c.KeyValueStore.SetWithEnvDuration(context.Background(), fmt.Sprintf("routing %s", sessionID), value, c.RetrievalDurationEnvVariable, time.Minute)
}

// Helper function to standardize deletion of routing key for messaging logic
func (c *Caching) DeleteRouting(sessionOrRoomID string) error {
	return c.KeyValueStore.Delete(context.Background(), fmt.Sprintf("routing %s", sessionOrRoomID))
}

func (c *Caching) AddToRoom(roomID *string, sessionID *string) error {
	log.Trace().Str("sessionID", *sessionID).Str("roomID", *roomID).Msg("adding session to room")
	roomRet, err := c.KeyValueStore.Get(context.Background(), fmt.Sprintf("room %s", *roomID), keyvaluestore.String)
	room := &models.RoomCache{
		RoomID:   *roomID,
		Sessions: []string{*sessionID},
	}
	if err != nil && err != keyvaluestore.ErrKeyNotFound {
		log.Error().Str("sessionID", *sessionID).Str("roomID", *roomID).Err(err).Msg("error getting room from redis")
		return err
	}
	if err == keyvaluestore.ErrKeyNotFound || roomRet == nil || roomRet.(string) == "" {
		log.Trace().Str("sessionID", *sessionID).Str("roomID", *roomID).Msg("creating new room")
	} else {
		log.Trace().Str("sessionID", *sessionID).Str("roomID", *roomID).Msg("adding session to existing room")
		json.Unmarshal([]byte(roomRet.(string)), room)
		add := true
		for _, s := range room.Sessions {
			if s == *sessionID {
				add = false
				break
			}
		}
		if add {
			room.Sessions = append(room.Sessions, *sessionID)
		}
	}

	b, _ := json.Marshal(room)
	return c.SetWithEnvDuration(context.Background(), fmt.Sprintf("room %s", *roomID), b, c.RetrievalDurationEnvVariable, time.Minute)
}

func (c *Caching) GetFromRoom(roomID *string) (models.RoomCache, error) {
	room, err := c.KeyValueStore.Get(context.Background(), fmt.Sprintf("room %s", *roomID), keyvaluestore.String)
	if err != nil && err != keyvaluestore.ErrKeyNotFound {
		log.Error().Str("sessionID", *roomID).Err(err).Msg("error getting room from redis")
		return models.RoomCache{}, err
	}
	if err == keyvaluestore.ErrKeyNotFound {
		log.Error().Str("sessionID", *roomID).Err(err).Msg("room not found")
		return models.RoomCache{}, err
	}

	ret := models.RoomCache{}
	json.Unmarshal([]byte(room.(string)), &ret)
	return ret, nil
}

func RemoveValueFromSlice(s []string, sessionID *string) []string {
	ret := make([]string, 0)
	for _, i := range s {
		if i != *sessionID {
			ret = append(ret, i)
		}
	}
	return ret
}

func (c *Caching) RemoveFromRoom(roomID *string, sessionID *string) error {
	roomB, err := c.KeyValueStore.Get(context.Background(), fmt.Sprintf("room %s", *roomID), keyvaluestore.String)
	if err != nil && err != keyvaluestore.ErrKeyNotFound {
		log.Error().Str("sessionID", *roomID).Err(err).Msg("error getting room from redis")
		return err
	}
	if err == keyvaluestore.ErrKeyNotFound {
		log.Error().Str("sessionID", *roomID).Err(err).Msg("room not found")
		return err
	}

	room := models.RoomCache{}
	json.Unmarshal([]byte(roomB.(string)), &room)
	room.Sessions = RemoveValueFromSlice(room.Sessions, sessionID)

	b, _ := json.Marshal(room)
	return c.KeyValueStore.SetWithEnvDuration(context.Background(), fmt.Sprintf("room %s", *roomID), b, c.RetrievalDurationEnvVariable, time.Minute)
}
