package caching

import (
	"context"
	"fmt"
	"time"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
	"github.com/rs/zerolog/log"
)

func (c *Caching) retrieveCached(key string) (map[string]interface{}, bool, error) {
	// Retrieve the schema from the object store
	// Create context with 30 second timeout
	getContext, cancelGet := context.WithTimeout(context.Background(), time.Second*30)
	defer cancelGet()
	data, err := c.KeyValueStore.GetMap(getContext, key)
	if err != nil {
		if err == keyvaluestore.ErrKeyNotFound {
			return nil, false, nil
		}
		log.Error().AnErr("Err", err).Msg("error accessing keyvaluestore")
		return nil, false, err
	}

	return data, true, err
}

func (c *Caching) retrieve(request *[]byte, keySuffix string, allowCached bool, message *models.MessageData, checkIfAlreadyBeingRetrieved bool) error {
	fullRetrievalKey := fmt.Sprintf("%s retrieval %s", c.Key, keySuffix) //sessionData.DatabaseName, sessionData.UserID)
	fullKey := fmt.Sprintf("%s %s", c.Key, keySuffix)
	log.Debug().Any("sessionData", message.SessionData).Str("fullRetrievalKey", fullRetrievalKey).Msg("retrieving")

	if allowCached {
		// Check if the result has been cached
		data, present, err := c.retrieveCached(fullKey)
		if err != nil {
			log.Error().AnErr("Err", err).Msg("error retrieving cached data")
			return err
		}
		if present && c.CacheHitCallback != nil {
			log.Trace().Msg("cache hit!")
			err = c.CacheHitCallback(data, message)
			if err != nil {
				return err
			}
			// if c.CacheHitCallback != nil {
			// 	err = c.CacheHitCallback(data, sessionData)
			// 	if err != nil {
			// 		return err
			// 	}
			// }

			err = c.KeyValueStore.Delete(context.Background(), fullRetrievalKey)
			if err != nil {
				log.Error().AnErr("err", err).Msg("failed to delete retrieval in redis cache")
			}

			return nil
		}
	}

	// Check if the schema should be force refreshed
	if !allowCached {
		log.Trace().Str("key", c.Key).Msg("Forced! Producing a retrieval request")
	} else {
		log.Trace().Str("key", c.Key).Msg("Cache Miss! Producing a retrieval request")
	}

	if checkIfAlreadyBeingRetrieved {
		fullRetrievalKey := fmt.Sprintf("%s retrieval %s", c.Key, keySuffix) //sessionData.DatabaseName, sessionData.UserID)
		beingRetrievedInterface, err := c.KeyValueStore.Get(context.Background(), fullRetrievalKey, keyvaluestore.Bool)

		// Check if the schema is currently being retrieved for this database
		if err != nil && err != keyvaluestore.ErrKeyNotFound {
			log.Error().AnErr("Err", err).Str("key", c.Key).Msg("error accessing keyvaluestore in retrieve method")
			return err
		}

		// Cast the interface{} to bool
		beingRetrieved, ok := beingRetrievedInterface.(bool)
		if !ok {
			log.Error().Str("key", c.Key).Msg("error casting beingRetrieved to bool")
			return err
		}

		// is already being retrieved
		if beingRetrieved {
			log.Warn().Str("key", c.Key).Msg("is already being retrieved")
			return nil
		}

		log.Trace().Str("key", c.Key).Msg("is not being retrieved yet")
	}

	// Produce a schema retrieval request
	data, err := c.CacheMissCallback(*request, message)
	if err != nil {
		log.Error().AnErr("Err", err).Msg("error producing schema retrieval request")
		return err
	}

	// Update KVS with a Create 30 second put context
	putSchemaContext, cancelSetSchema := context.WithTimeout(context.Background(), time.Second*30)

	if data != nil {
		err = c.KeyValueStore.Delete(putSchemaContext, fullRetrievalKey)
		if err != nil {
			log.Error().AnErr("err", err).Msg("failed to set retrieval in redis cache")
		}

		err = c.KeyValueStore.SetWithEnvDuration(putSchemaContext, fullKey, data, c.RetrievalDurationEnvVariable, time.Minute)
		if err != nil {
			log.Error().Any("data", data).AnErr("err", err).Msg("failed to set data in redis cache")
		} else {
			log.Trace().Any("fullKey", fullKey).Msg("added data to cache!")
		}
	} else {
		// if there is no data in the cachemiss return, assume it is async and update the cache already being retreived
		err = c.KeyValueStore.SetWithEnvDuration(putSchemaContext, fullRetrievalKey, true, c.RetrievalDurationEnvVariable, time.Minute)
		if err != nil {
			log.Error().AnErr("err", err).Msg("failed to set retrieval in redis cache")
		}
	}
	cancelSetSchema()

	return nil
}
