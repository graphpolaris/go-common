# Broker Driver
This is the broker driver package. It is used in almost every service as the way to access RabbitMQ. The `Interface` in `interface.go` is implemented by both the actual driver and the mock driver. This allows for the use of a mock broker in usecase testing in services.

## Creating a Driver
```go
import "git.science.uu.nl/datastrophe/broker"

broker := broker.NewDriver()
```

## Creating a Mock Driver
```go
import "git.science.uu.nl/datastrophe/broker"

mockBroker := broker.NewMockDriver()
```

This `mockBroker` can also be cast explicitly to a `MockDriver` to allow for access to messages that were sent.

```go
castMockBroker := mockBroker.(*broker.MockDriver)
```

## Producers and Consumers
Producers and consumers can be made using the driver.

### Creating Producers
```go
producer := broker.CreateProducer("exchange-name")
```
To create a producer, only the exchange name has to be passed. To publish a message the `PublishMessage(body *[]byte, routingKey string, headers *map[string]interface{})` method can be called.

### Creating Consumers
```go
consumer = s.brokerDriver.CreateConsumer("requests-exchange", "arangodb-query-queue", "arangodb-query-request")

consumer.SetMessageHandler(s.HandleMessage)

consumer.ConsumeMessages()
```
To create a consumer, the exchange name, queue name and routing key need to be given. Then a message handler needs to be given to the consumer. This message handler function should have type `func(msg *Message)`. To start receiving messages the `ConsumeMessages()` method needs to be called. This starts a goroutine, so keep in mind that this is a non-blocking call.