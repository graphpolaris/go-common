package producer

import (
	"encoding/json"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
)

/*
PublishAnalyticsRequest will publish the message to the queue retrieved from the key value store, with the given sessionID, to the GSA

	data: *[]byte, the data to publish
	sessionID: *string, the ID of the session
*/
func (ap *BrokerAliceProducer) PublishAnalyticsRequest(msg models.ToFrontend, message *models.MessageData, routingKey string) {
	b, _ := json.Marshal(msg)
	ap.PublishMessageJson(&b, "gsa-request", message)
}
