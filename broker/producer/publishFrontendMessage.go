package producer

import (
	"encoding/json"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
)

/*
PublishSchemaResult will publish the message to the queue retrieved from the key value store, with the given sessionID, to the UI

	data: *[]byte, the data to publish
	sessionID: *string, the ID of the session
*/
func (ap *BrokerAliceProducer) PublishFrontendMessage(msg models.ToFrontend, message *models.MessageData, routingKey string) {
	headers := ap.MessageHeaders(message)
	headers["origin"] = ap.serviceName

	msg.CallID = message.FromFrontend.CallID
	b, _ := json.Marshal(msg)

	ap.PublishMessageJson(&b, routingKey, message)
}

func (ap *BrokerAliceProducer) PublishFrontendSuccessMessage(typeVal string, value interface{}, message *models.MessageData, routingKey string) {
	msg := models.ToFrontend{
		Type:   typeVal,
		Value:  value,
		Status: "success",
	}

	ap.PublishFrontendMessage(msg, message, routingKey)
}
