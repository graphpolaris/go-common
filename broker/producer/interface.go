/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package producer

import (
	"encoding/json"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"github.com/christinoleo/alice"
)

/*
A Producer belongs to a broker and publishes messages to a queue
*/
type BrokerProducerI interface {
	PublishMessageJsonHeaders(body *[]byte, routingKey string, headers *map[string]interface{}) BrokerProducerI
	PublishMessageJson(body *[]byte, routingKey string, message *models.MessageData) BrokerProducerI
	PublishErrorMessage(err error, sessionID, routingKey string)
	PublishAnalyticsRequest(msg models.ToFrontend, message *models.MessageData, routingKey string)
	PublishFrontendMessage(msg models.ToFrontend, message *models.MessageData, routingKey string)
	PublishFrontendSuccessMessage(typeVal string, value interface{}, message *models.MessageData, routingKey string)
	MessageHeaders(message *models.MessageData) map[string]interface{}
}

/*
BrokerAliceProducer implements the producer interface
*/
type BrokerAliceProducer struct {
	producer alice.Producer
	// analyticsProducer alice.Producer
	serviceName string
}

func NewBrokerAliceProducer(producer alice.Producer, analyticsProducer alice.Producer, serviceName string) BrokerProducerI {
	return &BrokerAliceProducer{
		producer: producer,
		// analyticsProducer: analyticsProducer,
		serviceName: serviceName,
	}
}

func (ap *BrokerAliceProducer) MessageHeaders(message *models.MessageData) map[string]interface{} {
	headers := make(map[string]interface{})
	m, err := json.Marshal(message)
	if err != nil {
		return headers
	}
	headers["message"] = m
	headers["origin"] = ap.serviceName
	headers["callid"] = message.FromFrontend.CallID

	return headers
}
