package producer

import (
	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"github.com/rs/zerolog/log"
)

/*
PublishMessage publishes a message with the specified routing key

	body: *[]byte, the message to send, most likely a marshalled JSON object
	routingKey: string, the key by which this message will be routed on the exchange
	headers: *map[string]interface{}, the headers for this message, containing things like userID and sessionID
*/
func (ap *BrokerAliceProducer) PublishMessageJsonHeaders(body *[]byte, routingKey string, headers *map[string]interface{}) BrokerProducerI {
	(*headers)["origin"] = ap.serviceName
	log.Trace().Str("routingKey", routingKey).Any("headers", headers).Msg("Publishing message")

	ap.producer.PublishMessageJsonHeaders(body, routingKey, headers)
	return ap
}

/*
PublishMessage publishes a message with the specified routing key

	body: *[]byte, the message to send, most likely a marshalled JSON object
	routingKey: string, the key by which this message will be routed on the exchange
*/
func (ap *BrokerAliceProducer) PublishMessageJson(body *[]byte, routingKey string, message *models.MessageData) BrokerProducerI {
	log.Trace().Str("routingKey", routingKey).Msg("Publishing message")
	headers := ap.MessageHeaders(message)

	ap.producer.PublishMessageJsonHeaders(body, routingKey, &headers)
	return ap
}
