/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package broker

import (
	"git.science.uu.nl/graphpolaris/go-common/broker/consumer"
	"git.science.uu.nl/graphpolaris/go-common/broker/producer"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
)

/*
Interface describes the broker driver
*/
type Interface interface {
	CreateConsumer(exchangeName string, queueName string, routingKey string, routingKeyStore keyvaluestore.Interface, serviceName string) consumer.BrokerConsumerI
	CreateConsumerWithClient(exchangeName string, queueName string, routingKey string, routingKeyStore keyvaluestore.Interface, serviceName string) (consumer.BrokerConsumerI, producer.BrokerProducerI)
	CreateProducer(exchangeName string, serviceName string) producer.BrokerProducerI
}
