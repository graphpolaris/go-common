package consumer

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
	"github.com/rs/zerolog/log"
)

func (ac *BrokerAliceConsumer) HandleRouting(msg *BrokerConsumerMessage) {
	// Grab the userID and sessionID from the message headers

	log.Trace().Any("message", msg.Headers).Msg("Consuming message! 1")
	message, err := getHeaders(msg)
	sessionData := message.SessionData
	if err != nil {
		log.Error().Str("userID", sessionData.UserID).Str("sessionID", sessionData.SessionID).Msg(err.Error())
		return
	}
	log.Trace().Any("sessionData", sessionData).Msg("Consuming message! 2")

	// Get the routingKey to this client from Redis
	routingKey, err := ac.getRoutingKey(sessionData.SessionID)
	if err != nil {
		log.Error().Str("userID", sessionData.UserID).Str("sessionID", sessionData.SessionID).Msg(err.Error())
		return
	}
	log.Trace().Any("sessionData", sessionData).Str("routingKey", routingKey).Msg("Consuming message! 3")

	// Defer a panic intercepting function
	defer func(sessionID string) {
		if err := recover(); err != nil {
			// check err type if is error
			err, ok := err.(error)
			if !ok {
				err = fmt.Errorf("%v", err)
			}

			log.Panic().AnErr("err", err).Msg("Error in message handler")

			// Send error message
			if ac.producer != nil {

				ac.producer.PublishErrorMessage(err, sessionID, routingKey)
			} else {
				log.Warn().Msg("No producer available to send error message")
			}
		}
	}(sessionData.SessionID)

	// Call the consumer message handler
	ac.messageHandler(msg, routingKey, message)

}

// getHeaders gets the userID and sessionID values from the message headers
func getHeaders(msg *BrokerConsumerMessage) (*models.MessageData, error) {
	m, ok := msg.Headers["message"]
	if !ok {
		err := errors.New("missing message in headers")
		return nil, err
	}
	mm := &models.MessageData{}
	err := json.Unmarshal(m.([]byte), &mm)
	if err != nil {
		log.Error().AnErr("err", err).Any("header", m).Msg("error unmarshalling message")
		return nil, err
	}

	return mm, nil
}

// getRoutingKey gets the routingKey for this session from Redis
func (ac *BrokerAliceConsumer) getRoutingKey(sessionID string) (queueID string, err error) {
	routingKeyVal, err := ac.routingCache.GetRouting(sessionID)
	if err != nil {
		return
	}

	queueID = routingKeyVal.QueueID

	// Check if there is an actual queue, if there is none don't publish a message
	if queueID == "" {
		log.Warn().Str("sessionID", sessionID).Msg("Unable to find routingKey in redis, waiting 2s")
		time.Sleep(2 * time.Second) // TODO: make so each service creates if not exist
		routingKeyVal, err = ac.routingCache.GetRouting(sessionID)
		if err != nil {
			return
		}

		queueID = routingKeyVal.QueueID
		if queueID == "" {
			err = errors.New(fmt.Sprintf("no routing key found for id %s", sessionID))
			return
		}
	}

	log.Trace().Str("sessionID", sessionID).Str("routingKey", queueID).Msg("found routingKey for session")

	return
}
