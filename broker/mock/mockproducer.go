/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package mock

import (
	"encoding/json"

	"git.science.uu.nl/graphpolaris/go-common/broker"
	"git.science.uu.nl/graphpolaris/go-common/broker/producer"
	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
)

/*
A MockProducer mocks the Producer interface
*/
type MockProducer struct {
	broker *MockDriver

	// The exchange this producer is connected to
	exchange string
}

/*
PublishMessage publishes a message to the given queue (mock)

	body: *[]byte, the body of the message
	routingKey: string, the routingkey of the message
	headers: *map[string]interface{}, the headers of the message
*/
func (mp *MockProducer) PublishMessageJsonHeaders(body *[]byte, routingKey string, headers *map[string]interface{}) producer.BrokerProducerI {
	// Create the message
	msg := broker.Message{
		Headers: *headers,
		Body:    *body,
	}

	// Append the message to the list
	mp.broker.Messages[routingKey] = append(mp.broker.Messages[routingKey], msg)
	return mp
}

func (mp *MockProducer) PublishMessageJson(body *[]byte, routingKey string, message *models.MessageData) producer.BrokerProducerI {
	return mp
}

func (mp *MockProducer) PublishErrorMessage(err error, sessionID, routingKey string) {
}
func (mp *MockProducer) PublishAnalyticsRequest(msg models.ToFrontend, message *models.MessageData, routingKey string) {
	headers := mp.MessageHeaders(message)
	b, _ := json.Marshal(msg)
	mp.PublishMessageJsonHeaders(&b, "gsa-request", &headers)
}

func (mp *MockProducer) PublishFrontendMessage(msg models.ToFrontend, message *models.MessageData, routingKey string) {
}
func (mp *MockProducer) PublishFrontendSuccessMessage(typeVal string, value interface{}, message *models.MessageData, routingKey string) {
}

func (ap *MockProducer) MessageHeaders(message *models.MessageData) map[string]interface{} {
	return map[string]interface{}{
		"message": message,
	}
}
