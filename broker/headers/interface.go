package brokerHeaders

import (
	"errors"

	"git.science.uu.nl/graphpolaris/go-common/broker/consumer"
	"git.science.uu.nl/graphpolaris/go-common/structs/v1/models"
)

func makeHeaders(message *models.MessageData) map[string]interface{} {
	headers := make(map[string]interface{})
	headers["message"] = message
	return headers
}

// getHeaders gets the userID and sessionID values from the message headers
func getHeaders(msg *consumer.BrokerConsumerMessage) (*models.MessageData, error) {
	message, ok := msg.Headers["message"]
	if !ok {
		err := errors.New("missing message in headers")
		return nil, err
	}
	return message.(*models.MessageData), nil
}
