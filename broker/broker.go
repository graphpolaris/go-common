/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package broker

import (
	"time"

	"git.science.uu.nl/graphpolaris/go-common/broker/consumer"
	"git.science.uu.nl/graphpolaris/go-common/broker/producer"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
	"github.com/christinoleo/alice"
	"github.com/rs/zerolog/log"
)

/*
Driver implements the broker interface
*/
type Driver struct {
	broker alice.Broker
}

/*
NewDriver creates a new broker driver

	Returns: *Driver, the driver for the RabbitMQ broker. Implements the broker Interface
*/
func NewDriver(username, password, address string, port int) Interface {
	// Create config
	config := alice.CreateConfig(username, password, address, port, true, time.Second*5)

	// Create the Alice RabbitMQ broker (blocking until connection is made)
	broker, err := alice.CreateBroker(config)
	if err != nil {
		log.Panic().AnErr("err", err).Msg("failed to create broker")
	}

	d := Driver{
		broker: broker,
	}

	return &d
}

/*
CreateConsumer creates a consumer with the specified parameters

	exchangeName: string, the name of the exchange to which this consumer will bind its queue
	queueName: string, the name of the queue this consumer will consume messages from
	routingKey: string, the routingKey this consumer listens to
	Returns: An object implementing the Consumer interface
*/
func (d *Driver) CreateConsumer(exchangeName string, queueName string, routingKey string, routingKeyStore keyvaluestore.Interface, serviceName string) consumer.BrokerConsumerI {
	// Declare the RabbitMQ exchange we want to bind our queue to
	exchange, err := alice.CreateDefaultExchange(exchangeName, alice.Direct)
	if err != nil {
		log.Panic().AnErr("err", err).Msg("failed to declare exchange")
	}

	// Declare the queue we will consume from
	queue := alice.CreateQueue(exchange, queueName, false, false, true, false, nil)

	// Create the consumer
	c, err := d.broker.CreateConsumer(queue, routingKey, "")
	if err != nil {
		log.Panic().AnErr("err", err).Msg("failed to create consumer")
	}

	return consumer.NewBrokerAliceConsumer(c, routingKeyStore, serviceName)
}

func (d *Driver) CreateConsumerWithClient(exchangeName string, queueName string, routingKey string, routingKeyStore keyvaluestore.Interface, serviceName string) (consumer.BrokerConsumerI, producer.BrokerProducerI) {
	consumer := d.CreateConsumer(exchangeName, queueName, routingKey, routingKeyStore, serviceName)
	producer := d.CreateProducer("ui-direct-exchange", serviceName)
	consumer.AddClientProducer(producer)

	return consumer, producer
}

/*
CreateProducer creates a producer with the specified parameters

	exchangeName: string, the name of the exchange to which this producer will publish messages
	Returns: An object implementing the Producer interface
*/
func (d *Driver) CreateProducer(exchangeName string, serviceName string) producer.BrokerProducerI {
	// Declare the exchange this producer will publish messages to
	exchange, err := alice.CreateDefaultExchange(exchangeName, alice.Direct)
	if err != nil {
		log.Panic().AnErr("err", err).Msg("failed to declare exchange")
	}

	// Create the producer
	p, err := d.broker.CreateProducer(exchange)
	if err != nil {
		log.Panic().AnErr("err", err).Msg("failed to create producer")
	}

	// Declare the exchange of the analytics producer
	analyticsExchange, err := alice.CreateDefaultExchange("gsa-direct-exchange", alice.Direct)
	if err != nil {
		log.Panic().AnErr("err", err).Msg("failed to declare analytics exchange")
	}

	// Create the analytics producer
	analytics, err := d.broker.CreateProducer(analyticsExchange)
	if err != nil {
		log.Panic().AnErr("err", err).Msg("failed to create analytics producer")
	}

	// analyticsProducerDriver = s.brokerDriver.CreateProducer("gsa-direct-exchange", "schema-service")

	return producer.NewBrokerAliceProducer(p, analytics, serviceName)
}
