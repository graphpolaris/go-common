/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/
package broker

/*
Message describes the message to be handled by the message consumer
*/
type Message struct {
	Headers map[string]interface{}
	Body    []byte
}
